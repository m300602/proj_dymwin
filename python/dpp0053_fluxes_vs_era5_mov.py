import sys
import numpy as np
import matplotlib 
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs
import cartopy
import glob, os
import xarray as xr

run = 'dpp0053'
path_data = f'/work/mh0287/k203123/GIT/icon-aes-dyw3/experiments/{run}/'
timi = pyic.timing([0], 'start')

savefig = True
path_fig = '../movies/%s/' % (__file__.split('/')[-1][:-3])
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass


fpath_ckdtree_atm = '/mnt/lustre01/work/mh0033/m300602/icon/grids/r2b9_atm_r0015/ckdtree/rectgrids/r2b9_atm_r0015_res0.30_180W-180E_90S-90N.nc'

mfdset_kwargs = dict(combine='nested', concat_dim='time', 
                     data_vars='minimal', coords='minimal', compat='override', join='override',
                     parallel=True,
                    )

# --- open era data set
timi = pyic.timing(timi, 'open ERA5 data')

#path_era = '/work/bm0944/m300525/mpiom/ERA5/SOURCE/era5_cdsapi/nextgems_hackathon/'
path_era = '/home/mpim/m300602/work/obs_data/era5/collection/'
flist = []
for fname in [
#     'era5_evap_1h_2020.nc',
    'era5_precip_1h_2020.nc',
    'era5_evap_1h_2020.nc',
    'era5_slhf_1h_2020_Wm-2.nc',
    'era5_sshf_1h_2020_Wm-2.nc',
    'era5_ssr_1h_2020_Wm-2.nc',
    'era5_str_1h_2020_Wm-2.nc',
    'era5_ustress_1h_2020_Nm-2.nc',
    'era5_vstress_1h_2020_Nm-2.nc',
    ]:
    flist.append(f'{path_era}{fname}')
ds_era = xr.open_mfdataset(flist, chunks=dict(time=1))
ds_era = ds_era.sel(time=slice('2020-01-20', None))
ds_era = ds_era.rename(dict(
    var147 = 'slhf',
    var146 = 'sshf',
    var176 = 'ssr',
    var177 = 'str',
    var180 = 'ustress',
    var181 = 'vstress',
    var182 = 'e',
    var228 = 'tp',
))
ds_era['abs_stress'] = xr.ufuncs.sqrt(ds_era['ustress']**2+ds_era['vstress']**2)
ds_era['abs_stress'] = ds_era['abs_stress'].assign_attrs({'long_name': 'wind stress magnitude'})
ds_era['emp'] = ds_era['e'] - ds_era['tp']
ds_era['emp'] = ds_era['emp'].assign_attrs({'long_name': 'evaporation minus precipitation'})

# --- prepare ERA5 interpolation
timi = pyic.timing(timi, 'prepare ERA5 interpolation')

fpath_ckdtree = '/home/mpim/m300602/work/obs_data/era5/from_pool/era5_res0.30.nc'
if False:
  Loni, Lati = np.meshgrid(ds_era.lon.data, ds_era.lat.data)
  loni = Loni.flatten()
  lati = Lati.flatten()
  
  lonr = np.arange(-180, 180, 0.3)
  latr = np.arange(-90, 90, 0.3)
  lono, lato = np.meshgrid(lonr, latr)
  lono = lono.flatten()
  lato = lato.flatten()
  
  nx = lonr.size
  ny = latr.size
  dckdtree, ickdtree = pyic.calc_ckdtree(loni, lati, lono, lato)
  dckdtree = dckdtree.reshape(ny, nx)
  ickdtree = ickdtree.reshape(ny, nx)
  
  ds_ckdt = xr.Dataset(coords=dict(lon=lonr, lat=latr))
  ds_ckdt['ickdtree_c'] = xr.DataArray(ickdtree, dims=['lat', 'lon'])
  ds_ckdt['dckdtree_c'] = xr.DataArray(dckdtree, dims=['lat', 'lon'])
  ds_ckdt.to_netcdf(fpath_ckdtree)
else:
  ds_ckdt = xr.open_dataset(fpath_ckdtree)
lon = ds_ckdt.lon
lat = ds_ckdt.lat

# --- open ICON data
timi = pyic.timing(timi, 'open ICON data')

flist = glob.glob(f'{path_data}{run}_atm_2d_ml_202001*.nc')
flist.sort()
flist = flist[:10]

ds = xr.open_mfdataset(flist, **mfdset_kwargs, chunks=dict(time=1))
ds['time'] = pyic.nctime_to_datetime64(ds.time.data, time_mode='float2date')
# ds = ds.isel(time=slice(2,None,4))
ds['rlts'] = ds['rlds']-ds['rlus']
ds['rlts'] = ds['rlts'].assign_attrs({'long_name': 'surface longwave radiation'})
ds['abs_stress'] = xr.ufuncs.sqrt(ds['tauu']**2+ds['tauv']**2)
ds['abs_stress'] = ds['abs_stress'].assign_attrs({'long_name': 'wind stress magnitude'})
ds['emp'] = ds['evspsbl'] - ds['pr']
ds['emp'] = ds['emp'].assign_attrs({'long_name': 'evaporation minus precipitation'})
ds = ds.isel(time=slice(0,None,2))

# --- translate between ICON and ERA5
Dcomp = dict(
    hfss = 'sshf',
    hfls = 'slhf',
    rsds = 'ssr', # surface downwelling shortwave radiation = surface_net_solar_radiation
    rlts = 'str', # surface upwelling longwave radiation = surface_net_thermal_radiation
    evspsbl = 'e',
    pr = 'tp',
    emp = 'emp',
    abs_stress = 'abs_stress',
    tauu = 'ustress',
    tauv = 'vstress',
)

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 
timi = pyic.timing(timi, 'prepare axes')

plt.close('all')
ccrs_proj = ccrs.PlateCarree()

ccrs_proj = ccrs.PlateCarree()
npx = 3
#plot_cb = np.zeros((npx*len(Dcomp)))
#plot_cb[npx-1::npx] = 1
plot_cb = np.zeros((npx*8))
plot_cb[npx-1::npx] = 1
hca, hcb = pyic.arrange_axes(2*npx, 4, asp=0.5, projection=ccrs_proj, fig_size_fac=1., 
                             sharex=True, sharey=True,
#                              plot_cb=True,
                             plot_cb = plot_cb,
                             dfigr=0.5,
                            )
ii=-1

# clim = 100
clim = 'sym'

Dclim = dict(
    hfss = 100,
    hfls = 100,
    rsds = 500,
    rlts = 100,
    pr = 1,
    evspsbl = 10.,
    emp = 10.,
    abs_stress = 0.2,
    tauu = 0.2,
    tauv = 0.2,
)

# cvars = ['rsds', 'rlts']
#cvars = ['hfss', 'hfls', 'rsds', 'rlts', 'emp', 'abs_stress', 'tauu', 'tauv']
cvars = ['hfss', 'hfls', 'rsds', 'rlts', 'pr', 'evspsbl', 'emp', 'abs_stress']
hm1 = [0]*len(cvars)
hm2 = [0]*len(cvars)
hm3 = [0]*len(cvars)
for nn, var in enumerate(cvars):
    var_era5 = Dcomp[var]
    #print(f'{var}: {var_era5}')
    timi = pyic.timing(timi, f'{var}: {var_era5}')
    it_icon = 1
    it_era5 = 1
    
    arr_tmp = ds_era[var_era5].isel(time=it_era5).stack(ncells=['lat', 'lon'])
    arri_era5 = pyic.interp_to_rectgrid_xr(arr_tmp, fpath_ckdtree)
    arri_icon = pyic.interp_to_rectgrid_xr(ds[var].isel(time=it_icon), fpath_ckdtree_atm)
    
    if var=='pr' or var=='emp' or var=='evspsbl':
        arri_era5 *= 86400./0.001 / 3600.
        arri_icon *= 86400.

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1[nn] = pyic.shade(lon, lat, arri_icon, ax=ax, cax=cax, clim=Dclim[var])
    ax.set_title(f'{run}', loc='left')
#     ax.set_title(ds[var].long_name)
    ax.set_title(f'{var}')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2[nn] = pyic.shade(lon, lat, arri_era5, ax=ax, cax=cax, clim=Dclim[var])
    ax.set_title(f'ERA5', loc='left')
    ax.set_title(f'{var_era5}')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm3[nn] = pyic.shade(lon, lat, arri_icon-arri_era5, ax=ax, cax=cax, clim=Dclim[var])
    ax.set_title(f'{run} - ERA5', loc='left')
    ax.set_title(f'{var}-{var_era5}')
    
    #cax.set_ylabel(ds[var].long_name)

ht = pyic.tbox(str(arri_icon.time.data)[:16], 'ul', hca[0])

# hcb[-1].set_ylabel(f'{arri_icon_0.long_name} [{arri_icon_0.units}]')

for ax in hca:
    pyic.plot_settings(ax, template='global')

#plt.show()
#sys.exit()

def update_fig(step):
  for nn, var in enumerate(cvars):
      var_era5 = Dcomp[var]
      #print(f'{var}: {var_era5}')
      #timi = pyic.timing(timi, f'{var}: {var_era5}')
      it_icon = step
      it_era5 = step
      
      arr_tmp = ds_era[var_era5].isel(time=it_era5).stack(ncells=['lat', 'lon'])
      arri_era5 = pyic.interp_to_rectgrid_xr(arr_tmp, fpath_ckdtree)
      arri_icon = pyic.interp_to_rectgrid_xr(ds[var].isel(time=it_icon), fpath_ckdtree_atm)
      
      if var=='pr' or var=='emp' or var=='evspsbl':
          arri_era5 *= 86400./0.001 / 3600.
          arri_icon *= 86400.
    
      hm1[nn][0].set_array(arri_icon.data.flatten())
      hm2[nn][0].set_array(arri_era5.data.flatten())
      hm3[nn][0].set_array((arri_icon-arri_era5).data.flatten())
  print(f'icon: {str(arri_icon.time.data)[:16]}, era5: {str(arri_era5.time.data)[:16]}')
    
  ht.set_text(str(arri_icon.time.data)[:16])
  return

steps = np.arange(ds.time.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  timi = pyic.timing(timi, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

timi = pyic.timing(timi, 'all done')

#plt.show()
