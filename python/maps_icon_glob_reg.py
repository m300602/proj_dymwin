import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.patches import Rectangle
#import my_toolbox as my
import cartopy.crs as ccrs
import cartopy
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import sys
import pyicon as pyic
#reload(pyic)

#lon_reg = [-75, -60]
#lat_reg = [30, 40]
#lon_reg = [-72, -70]
#lat_reg = [33, 35]
dpi = 250
#dpi = 500.

def make_axes(dpi):
  hca = []
  hcb = []
  lon_regs = []
  lat_regs = []

  # --- figure
  length = 4096./dpi
  #aspf = 0.527
  aspf = 0.5
  fig = plt.figure(figsize=(length,length*aspf))
  
  ccrs_proj = ccrs.PlateCarree()
  #ccrs_proj = None
  
  ### --- 1st axes
  ##height = 0.5
  ##asp = 0.52*aspf / aspf
  ##ax = fig.add_axes([0.04, 0.5, height*asp, height], projection=ccrs_proj)
  ##hca.append(ax)
  ##
  ##ax.set_xlim([-180., 180.])
  ##ax.set_ylim([-90., 90])
  ##
  ###if ccrs_proj!='none':
  ##if not ccrs_proj is None:
  ##  #ax.coastlines()
  ##  ax.add_feature(cartopy.feature.LAND, zorder=0, facecolor='0.9')
  ##  ax.set_xticks(np.arange(-180.,180.,60.), crs=ccrs_proj)
  ##  ax.set_yticks(np.arange(-90.,90.,30.), crs=ccrs_proj)
  ##  lon_formatter = LongitudeFormatter()
  ##  lat_formatter = LatitudeFormatter()
  ##  ax.xaxis.set_major_formatter(lon_formatter)
  ##  ax.yaxis.set_major_formatter(lat_formatter)
  ##  #ax.stock_img()
  ##ax.xaxis.set_ticks_position('both')
  ##ax.yaxis.set_ticks_position('both')
  ##ax.set_title('ICON-O', loc='left')
  
  #lw = 2.
  #ax.add_patch(
  #  Rectangle(xy=[lon_reg_2[0], lat_reg_2[0]], 
  #            width=lon_reg_2[1]-lon_reg_2[0], height=lat_reg_2[1]-lat_reg_2[0],
  #            edgecolor='k',
  #            facecolor='none',
  #            linewidth=lw,
  #            transform=ccrs_proj) )
  #
  #ax.add_patch(
  #  Rectangle(xy=[lon_reg_3[0], lat_reg_3[0]], 
  #            width=lon_reg_3[1]-lon_reg_3[0], height=lat_reg_3[1]-lat_reg_3[0],
  #            edgecolor='r',
  #            facecolor='none',
  #            linewidth=lw,
  #            transform=ccrs_proj) )
  
  bbox=dict(facecolor='w', alpha=1., edgecolor='none')
  #ax.text(0.02, 0.04, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox)

  aspo = 0.5
  if ccrs_proj is None:
    asp = aspo / aspf
  else:
    #asp = aspo
    asp = aspo / aspf
  length = 0.4

  # --- southern ocean
  lat_reg = [-90, -10]
  lon_reg = [-80, 30]
  lon_reg[1] = (lat_reg[1]-lat_reg[0])/aspo + lon_reg[0] 
  ax = fig.add_axes([0.02, 0.04, length, length*asp], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg)
  ax.set_ylim(lat_reg)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('SA')
  print((lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0]))
  print(ax.get_position().height / ax.get_position().width)
  lon_regs.append(lon_reg)
  lat_regs.append(lat_reg)

  # --- tropical atlantic
  lat_reg = [5, 35]
  lon_reg = [-100, -50]
  lon_reg[1] = (lat_reg[1]-lat_reg[0])/aspo + lon_reg[0] 
  ax = fig.add_axes([0.52, 0.04, length, length*asp], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg)
  ax.set_ylim(lat_reg)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('TA')
  print((lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0]))
  print(ax.get_position().height / ax.get_position().width)
  lon_regs.append(lon_reg)
  lat_regs.append(lat_reg)

  # --- sub-tropical atlantic
  lat_reg = [20, 55]
  lon_reg = [-80, -30]
  lon_reg[1] = (lat_reg[1]-lat_reg[0])/aspo + lon_reg[0]
  ax = fig.add_axes([0.52, 0.52, length, length*asp], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg)
  ax.set_ylim(lat_reg)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('STA')
  print((lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0]))
  print(ax.get_position().height / ax.get_position().width)
  lon_regs.append(lon_reg)
  lat_regs.append(lat_reg)
  
  # --- north atlantic
  lat_reg = [45, 70]
  lon_reg = [-66, -8]
  lon_reg[1] = (lat_reg[1]-lat_reg[0])/aspo + lon_reg[0] 
  ax = fig.add_axes([0.02, 0.52, length, length*asp], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg)
  ax.set_ylim(lat_reg)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('NA')
  print((lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0]))
  print(ax.get_position().height / ax.get_position().width)
  lon_regs.append(lon_reg)
  lat_regs.append(lat_reg)

  # --- global
  length = 0.3
  lat_reg = [-90, 90]
  lon_reg = [-180, -180]
  lon_reg[1] = (lat_reg[1]-lat_reg[0])/aspo + lon_reg[0] 
  ax = fig.add_axes([0.32, 0.35, length, length*asp], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg)
  ax.set_ylim(lat_reg)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('NA')
  print((lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0]))
  print(ax.get_position().height / ax.get_position().width)
  lon_regs.append(lon_reg)
  lat_regs.append(lat_reg)

  if not ccrs_proj is None:
    for ax in hca:
      ax.coastlines()
      ax.add_feature(cartopy.feature.LAND, zorder=0, facecolor='0.9')
      #ax.set_aspect('auto') 
      hcb.append(0)
  
  ## --- colorbar
  #height = 0.2
  #asp = 0.1
  ##cax = fig.add_axes([0.05, 0.48, height*asp, height])
  #cax = 0
  ##cax.yaxis.tick_right()
  ##cax.yaxis.set_label_position("right")
  ##cax.set_xticks([])
  #hcb.append(cax)

  #cax = fig.add_axes([0.75, 0.52, height, height*asp])
  #hcb.append(cax)

  #hcb.append(0)
  
  #ax.text(0.005, 0.01, 'ICON-O Submesoscale Telescope', transform=fig.transFigure)
  return fig, hca, hcb, lon_regs, lat_regs

# ---
if False:
  plt.close('all')
  
  # --- call fig constructor
  fig, hca, hcb = make_axes(dpi)
  ii=-1

  #ii+=1; ax=hca[ii]; cax=hcb[ii]
  #x = np.linspace(lon_reg_1[0],lon_reg_1[1],101)
  #y = np.linspace(lat_reg_1[0],lat_reg_1[1],201)
  #X, Y = np.meshgrid(x,y)
  #Z = np.cos(X*np.pi/180.)*np.sin((Y*np.pi/180.)**2)
  #pyic.shade(x, y, Z, ax=ax, cax=cax, clim='auto')
  #cax.set_ylabel('temperature [$^o$C]')

  #ii+=1; ax=hca[ii]; cax=hcb[ii]
  #x = np.linspace(lon_reg_1[0],lon_reg_1[1],101)
  #y = np.linspace(lat_reg_1[0],lat_reg_1[1],201)
  #X, Y = np.meshgrid(x,y)
  #Z = np.cos(X*np.pi/180.)*np.sin((Y*np.pi/180.)**2)
  #hm = pyic.shade(x, y, Z, ax=ax, cax=0, clim='auto')
  #plt.colorbar(mappable=hm[0], cax=cax, orientation='horizontal')
  #cax.set_xlabel('temperature [$^o$C]')
  
  print('saving')
  #plt.savefig('../pics/smt_map_test.jpg', dpi=dpi) 
  #
  plt.show()
  #sys.exit()

