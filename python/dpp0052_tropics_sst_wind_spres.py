import sys
import numpy as np
import matplotlib 
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs
import cartopy
import glob, os
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

run = 'dpp0052'
path_data = f'/work/mh0287/k203123/GIT/icon-aes-dyw3/experiments/{run}/'
timi = pyic.timing([0], 'start')

savefig = True
path_fig = '../movies/%s/' % (__file__.split('/')[-1][:-3])
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass

fpath_tgrid = path_data+'icon_grid_0015_R02B09_G.nc'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9a_res0.10_180W-180E_90S-90N.npz'

# --- load times and flist
search_str = run+'_atm_2d_ml_????????????????.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
flist = flist[:-1]
times, flist_ts, its = pyic.get_timesteps(flist, time_mode='float2date')
#times, flist_ts, its = pyic.get_timesteps(flist, time_mode='num2date')
nskip = 12
times = times[::nskip]
flist_ts = flist_ts[::nskip]
its = its[::nskip]

#it1 = 238
#it2 = 407
#times = times[it1:it2]
#flist_ts = flist_ts[it1:it2]
#its = its[it1:it2]

#search_str = run+'

# --- tropics
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi
clat = f.variables['clat'][:] * 180./np.pi
cell_sea_land_mask = f.variables['cell_sea_land_mask'][:]
f.close()
ind_trop = (clat>-30.) & (clat<30.)

clont = clon[ind_trop]
clatt = clat[ind_trop]
cell_sea_land_maskt = cell_sea_land_mask[ind_trop]
iwater = cell_sea_land_mask==-2
iwatert = cell_sea_land_maskt==-2

# --- load data
step = 50 # dummy step
timi = pyic.timing(timi, 'load data')
f = Dataset(flist_ts[step], 'r')
psl = f.variables['psl'][its[step],:]/100.
sfcwind = f.variables['sfcwind'][its[step],0,:]
ts = f.variables['ts'][its[step],:] - 273.15
f.close()
psl[iwater==False] = np.ma.masked
sfcwind[iwater==False] = np.ma.masked
ts[iwater==False] = np.ma.masked

# --- load ckdtree
timi = pyic.timing(timi, 'apply ckdtree')
ddnpz = np.load(fpath_ckdtree)
lon_rg = ddnpz['lon'] 
lat_rg = ddnpz['lat'] 

# --- interpolate to rectgrid
ts_rg = pyic.apply_ckdtree(ts, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
ts_rg[ts_rg==0.] = np.ma.masked
psl_rg = pyic.apply_ckdtree(psl, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
psl_rg[psl_rg==0.] = np.ma.masked
sfcwind_rg = pyic.apply_ckdtree(sfcwind, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
sfcwind_rg[sfcwind_rg==0.] = np.ma.masked

pcrit = 1005
wcrit = 12

def find_tc(psl, sfcwind):
  pslt = psl[ind_trop]
  sfcwindt = sfcwind[ind_trop]

  #icrit_all = np.ma.where((pslt<pcrit) & (sfcwindt>wcrit))[0]
  #for ii in icrit_all:
  #  clont[icrit_all]-clont[ii]

  #pslt[iwatert] = np.ma.masked
  #sfcwindt = sfcwindt[iwatert]
  itc = np.array([])
  pmintc = np.array([])
  wmaxtc = np.array([])
  while True:
    #imin = np.argmin(pslt)
    #icrit = imin
    imax = np.argmax(sfcwindt)
    icrit = imax
    pmin = pslt[icrit]
    wmax = sfcwindt[icrit]
    print(f'{clont[icrit]}, {clatt[icrit]}, {pslt[icrit]:.0f}, {sfcwindt[icrit]:.0f}')
    if pmin<pcrit and wmax>wcrit:
      print('taken')
      itc = np.array(list(itc)+[icrit])
      pmintc = np.array(list(pmintc)+[pmin])
      wmaxtc = np.array(list(wmaxtc)+[wmax])
    else:
      if wmax<wcrit:
        break
    imask = ((clont-clont[icrit])**2+(clatt-clatt[icrit])**2) < (6.)**2
    pslt[imask] = np.ma.masked
    sfcwindt[imask] = np.ma.masked
  return itc, pmintc, wmaxtc

itc, pmintc, wmaxtc = find_tc(psl, sfcwind)

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 
plt.close('all')
ccrs_proj = ccrs.PlateCarree()

timi = pyic.timing(timi, 'making axes')
#dpi = 250
hca, hcb = pyic.arrange_axes(1,3, plot_cb=True, fig_size_fac=1., asp=0.167, projection=ccrs_proj)
ii=-1

timi = pyic.timing(timi, 'reg_1')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm1 = pyic.shade(lon_rg, lat_rg, sfcwind_rg, ax=ax, cax=cax, clim=[0,20], 
                transform=ccrs_proj, rasterized=False)
ax.set_title('wind speed [m/s]')
bbox=dict(facecolor='w', alpha=1., edgecolor='none')
#ht = ax.text(0.02, 0.92, times[0], transform=ax.transAxes, bbox=bbox)
ht = ax.set_title(times[0], loc='left')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm2 = pyic.shade(lon_rg, lat_rg, psl_rg, ax=ax, cax=cax, clim=[1000,1030], 
                transform=ccrs_proj, rasterized=False)
ax.set_title('mean sea level pressure [hPa]')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm3 = pyic.shade(lon_rg, lat_rg, ts_rg, ax=ax, cax=cax, clim=[23,33], 
                transform=ccrs_proj, rasterized=False)
ax.set_title('surface temperature [$^o$C]')
ax.text(0.02, 0.01, path_data, ha='left', va='bottom', transform=plt.gcf().transFigure, fontsize=10)

# --- make axes nice
hc = [0]*3
hs = [0]*3
htt = [0]*3
for nn, ax in enumerate(hca):
  hs[nn] = ax.scatter(clont[itc], clatt[itc], s=2, c='k')
  #ax.axhline(15, color='k')
  #ax.axhline(-15, color='k')
  htmp = []
  for kk in range(itc.size):
    htmp.append( ax.text(clont[itc[kk]], clatt[itc[kk]], f'{pmintc[kk]:.0f}, {wmaxtc[kk]:.0f}') )
  htt[nn] = htmp

for ax in hca:
  pyic.plot_settings(ax, xlim=[-180,180], ylim=[-30,30], land_facecolor='0.7')

def update_fig(step, hs, htt):
  # --- load data
  f = Dataset(flist_ts[step], 'r')
  sfcwind = f.variables['sfcwind'][its[step],0,:]
  psl = f.variables['psl'][its[step],:]/100.
  ts = f.variables['ts'][its[step],:] - 273.15
  f.close()
  # --- interp to rectgrid 
  sfcwind_rg = pyic.apply_ckdtree(sfcwind, fpath_ckdtree, coordinates='clat clon', 
    radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  sfcwind_rg[sfcwind_rg==0.] = np.ma.masked
  psl_rg = pyic.apply_ckdtree(psl, fpath_ckdtree, coordinates='clat clon', 
    radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  psl_rg[psl_rg==0.] = np.ma.masked
  ts_rg = pyic.apply_ckdtree(ts, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  ts_rg[ts_rg==0.] = np.ma.masked

  itc, pmintc, wmaxtc = find_tc(psl, sfcwind)

  # --- remove old dots and labels
  try:
    for nn in range(3):
      hs[nn].remove()
      for htmp in htt[nn]:
        htmp.remove()
  except:
    print('Could not remove text and scatter points.')

  # --- add new dots and labels
  print(f'itc = {itc}')
  if itc.size>0:
    hs = [0]*3
    htt = [0]*3
    for nn, ax in enumerate(hca):
      hs[nn] = ax.scatter(clont[itc], clatt[itc], s=2, c='k')
      #ax.axhline(15, color='k')
      #ax.axhline(-15, color='k')
      htmp = []
      for kk in range(itc.size):
        htmp.append( ax.text(clont[itc[kk]], clatt[itc[kk]], f'{pmintc[kk]:.0f}, {wmaxtc[kk]:.0f}') )
      htt[nn] = htmp
  else:
    print('No points to plot.')

  # --- update
  #hm1[0].set_array(sfcwind_rg[1:,1:].flatten())
  #hm2[0].set_array(psl_rg[1:,1:].flatten())
  #hm3[0].set_array(ts_rg[1:,1:].flatten())
  hm1[0].set_array(sfcwind_rg.flatten())
  hm2[0].set_array(psl_rg.flatten())
  hm3[0].set_array(ts_rg.flatten())
  ht.set_text(times[step])
  return hs, htt

steps = np.arange(flist_ts.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
print(f'times = {times}')
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  print(f'times[step] = {times[step]}')
  timi = pyic.timing(timi, 'loop step')

  hs, htt = update_fig(step, hs, htt)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

timi = pyic.timing(timi, 'all done')

#plt.show()
