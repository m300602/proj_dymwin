import sys
import numpy as np
import matplotlib.pyplot as plt
import os, shutil, glob

class SimList(object):
  def __init__(self, run='', path_data=''):
    self.slist = dict()
    return
  def add(self, run='', path_data=''):
    self.slist[run] = Simulation(
      run=run,
      path_data=path_data,
    )
    return
  def __getitem__(self, item):
    return self.slist[item]

class Simulation(object):
  def __init__(self, run='', path_data=''):
    self.run = run
    self.path_data = path_data
    return

Ss = SimList()

run = 'dpp0060'
Ss.add(
  run=run,
  path_data=f'/mnt/lustre01/work/mh0287/mh0469/m211054/projects/nextgems/nextgems_cycle1_zstar_diag/experiments/{run}/',
)

run = 'dpp0062'
Ss.add(
  run=run,
  path_data=f'/work/mh0287/mh0469/m211054/projects/nextgems/nextgems_cycle1_zstar_diag_revert_atmosphere/experiments/{run}/'
)

run = 'dpp0063'
Ss.add(
  run=run,
  path_data=f'/mnt/lustre01/work/mh0287/mh0469/m211054/projects/nextgems/nextgems_cycle1_zstar_diag/experiments/{run}/',
)

run = 'dpp0065'
Ss.add(
  run=run,
  path_data=f'/mnt/lustre01/work/mh0287/mh0469/m211054/projects/nextgems/nextgems_cycle1_zstar_diag/experiments/{run}/',
)
