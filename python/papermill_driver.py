import papermill as pm

#pm.inspect_notebook('./papermill_test.ipynb')

#params = dict(
#  run = 'exp.ocean_era51h_zstar_r2b8_21070-SMT'
#  path_data = f'/work/bm1102/m211054/smtwave/zstar2/experiments/{run}/outdata/'
#
#  fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b8_res0.30_180W-180E_90S-90N.npz'
#)

class Simulation(object):
  def __init__(self):
    return

Sims = []

S = Simulation()
S.run = 'exp.ocean_era51h_zstar_r2b9_21223-DWS'
S.path_data = f'/work/bm1102/m211054/dyamond/zstar2/experiments/{S.run}/outdata/'
S.fpath_ckdtree = '/home/mpim/m300602/work/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.30_180W-180E_90S-90N.npz'
Sims.append(S)

#S = Simulation()
#S.run = 'exp.ocean_era51h_zstar_r2b9_21182-DYW'
#S.path_data = f'/work/bm1102/m211054/dyamond/zstar2_/experiments/{S.run}/outdata/'
#S.fpath_ckdtree = '/home/mpim/m300602/work/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.30_180W-180E_90S-90N.npz'
#Sims.append(S)

for nn, S in enumerate(Sims):
  print(f'nn = {nn}, run = {S.run}')
  params = dict(
    run=S.run, path_data=S.path_data, fpath_ckdtree=S.fpath_ckdtree,
  )

  notebook = pm.execute_notebook(
     './papermill_test.ipynb',
     './papermill_test_out.ipynb',
     parameters=params,
  )
