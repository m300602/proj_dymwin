import sys
import pyicon as pyic
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import os, shutil, glob

# -------------------------------------------------------------------------------- 
fbase = sys.argv[0][:-3]
path_scratch = f'/scratch/m/m300602/dymwin/'
#run = 'dpp0060'
run = sys.argv[2]
path_data = f'/mnt/lustre01/work/mh0287/mh0469/m211054/projects/nextgems/nextgems_cycle1_zstar_diag/experiments/{run}/'

mfdset_kwargs = dict(combine='nested', concat_dim='time',
                     data_vars='minimal', coords='minimal', 
                     compat='override', join='override',
                     parallel=True,
                    )
# -------------------------------------------------------------------------------- 

ts = pyic.timing([0], string='', verbose=True)
if len(sys.argv)>1 and sys.argv[1]=='--batch':
  
  ts = pyic.timing(ts, string='initialize cluster', verbose=True)
  from dask_mpi import initialize
  initialize()
  
  from dask.distributed import Client
  from distributed.scheduler import logger
  import socket
  #client = Client(local_directory=path_scratch)
  client = Client()

  host = client.run_on_scheduler(socket.gethostname)
  port = client.scheduler_info()['services']['dashboard']
  login_node_address = "supercomputer.university.edu" # Provide address/domain of login node
  #logger.info(f"ssh -N -L {port}:{host}:{port} {login_node_address}")
  logger.info(f"ssh -L 8788:localhost:{port} -J m300602@mistral.dkrz.de m300602@{host}")

ts = pyic.timing(ts, string='open tgrid file', verbose=True)
fpath_tgrid = '/mnt/lustre01/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/r2b9_oce_r0004_tgrid.nc'
ds_tg = xr.open_dataset(fpath_tgrid)
clon = ds_tg.clon*180./np.pi
clat = ds_tg.clat*180./np.pi
clon = clon.data
clat = clat.data

# lon0 = np.array([-140, -30, 0, -180, -140])
# lat0 = np.array([-30, 26, -60, -15, 0])
lon0 = np.array([-140])
lat0 = np.array([-30])

ic = np.argmin( (clon-lon0[:,np.newaxis])**2 + (clat-lat0[:,np.newaxis])**2, axis=1)
clon[ic], clat[ic]

# ---
ts = pyic.timing(ts, string='ds_3d', verbose=True)
flist = glob.glob(f'{path_data}/{run}_oce_3du200m_PT3H_202001*.nc')
flist.sort()
ds_3d = xr.open_mfdataset(flist, **mfdset_kwargs, chunks=dict(time=1, depth=1, depth_2=1))

ds_3d = ds_3d[['to', 'so', 'tke', 'u', 'v', 'A_tracer_v_to']]
ds_3d_sel = ds_3d.isel(ncells=ic)

ts = pyic.timing(ts, string='computation', verbose=True)
ds = ds_3d_sel.compute()

ts = pyic.timing(ts, string='storring', verbose=True)
fpatho = f'{path_scratch}/{fbase}_3d_{run}.nc'
print(f'Saving file {fpatho}')
ds.to_netcdf(fpatho)

# ---
ts = pyic.timing(ts, string='ds_tke', verbose=True)
flist = glob.glob(f'{path_data}/{run}_oce_tkeu200m_PT3H_202001*.nc')
flist.sort()
ds_tke = xr.open_mfdataset(flist, **mfdset_kwargs, chunks=dict(time=1, depth=1, depth_2=1))
ds_tke = ds_tke.rename(depth='depth_2')

ds_tke_sel = ds_tke.isel(ncells=ic)

ts = pyic.timing(ts, string='computation', verbose=True)
ds = ds_tke_sel.compute()

ts = pyic.timing(ts, string='storring', verbose=True)
fpatho = f'{path_scratch}/{fbase}_tke_{run}.nc'
print(f'Saving file {fpatho}')
ds.to_netcdf(fpatho)

# ---
ts = pyic.timing(ts, string='ds_tsbal', verbose=True)
flist = glob.glob(f'{path_data}/{run}_oce_tsbalu200m_PT3H_202001*.nc')
flist.sort()
ds_tsbal = xr.open_mfdataset(flist, **mfdset_kwargs, chunks=dict(time=1, depth=1, depth_2=1))

ds_tsbal_sel = ds_tsbal.isel(ncells=ic)

ts = pyic.timing(ts, string='computation', verbose=True)
ds = ds_tsbal_sel.compute()

ts = pyic.timing(ts, string='storring', verbose=True)
fpatho = f'{path_scratch}/{fbase}_tsbal_{run}.nc'
print(f'Saving file {fpatho}')
ds.to_netcdf(fpatho)

