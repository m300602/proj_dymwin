import sys
import numpy as np
import matplotlib 
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs
import cartopy
import glob, os
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import maps_icon_glob_reg as ma
import pandas as pd

run = 'dpp0052'
path_data = f'/work/mh0287/k203123/GIT/icon-aes-dyw3/experiments/{run}/'
snap_freq = 1

ts = pyic.timing([0], 'start')

savefig = True
#path_fig = '../movies/%s_%s/' % (__file__.split('/')[-1][:-3], run)
path_fig = '../movies/%s/' % (__file__.split('/')[-1][:-3])
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass

gname = 'OceanOnly_IcosSymmetric_4932m_rotatedZ37d_modified_srtm30_1min'
fpath_tgrid = '/pool/data/ICON/oes/input/r0002/'+gname+'/'+gname+'.nc'
#fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9_res0.05_180W-180E_90S-90N.npz'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9_res0.10_180W-180E_90S-90N.npz'
#fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9_res0.30_180W-180E_90S-90N.npz'

# --- load times and flist
search_str = f'{run}_oce_3dlev_P1D_????????????????.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
flist = flist[:-1]
times, flist_ts, its = pyic.get_timesteps(flist, time_mode='num2date')

# --- load data
step = 10 # dummy step
iz = 1
ts = pyic.timing(ts, 'load data')
f = Dataset(flist_ts[step], 'r')
uo = f.variables['u'][its[step],iz,:]
vo = f.variables['v'][its[step],iz,:]
kin = 0.5*(uo**2+vo**2)
#kin = f.variables['kin'][its[step],iz,:]
depth = f.variables['depth'][:]
f.close()
#ds = xr.open_dataset(flist_ts[step])

# --- interpolate to rectgrid
ts = pyic.timing(ts, 'apply ckdtree')
ddnpz = np.load(fpath_ckdtree)
lon_rg = ddnpz['lon'] 
lat_rg = ddnpz['lat'] 
kin_rg = pyic.apply_ckdtree(kin, fpath_ckdtree, coordinates='clat clon').reshape(lat_rg.size, lon_rg.size)
kin_rg[kin_rg==0.] = np.ma.masked

kin_rg = np.ma.log10(kin_rg)

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 
plt.close('all')
ccrs_proj = ccrs.PlateCarree()

ts = pyic.timing(ts, 'making axes')

fig, hca, hcb, lon_regs, lat_regs = ma.make_axes(dpi=250)
ii=-1

nn = 0
hcb[nn] = plt.axes((0.46,0.02,0.02,0.3)) 
hcb[nn].tick_params(labelsize=8)
hcb[nn].set_xticks([])
hcb[nn].yaxis.tick_right()
hcb[nn].yaxis.set_label_position("right")

hca[3].set_title(f'ICON-O {run}', loc='left')
htt = hca[2].set_title(times[0], loc='left')
hca[2].set_title(f'depth = {depth[iz]}m', loc='right')

ts = pyic.timing(ts, 'reg_1')

clims = [[-4,0]]*5
# ---
hm = [0]*len(hca)
for ax in hca:
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  hm[ii] = pyic.shade(lon_rg, lat_rg, kin_rg, ax=ax, cax=cax, 
                   clim=clims[ii], 
                   transform=ccrs_proj, rasterized=False, adjust_axlims=False, cmap='RdBu_r')
  ax.set_xlim(lon_regs[ii])
  ax.set_ylim(lat_regs[ii])
  #ht = ax.set_title(clims[ii])
  #ax.set_title('SSH clim: '+ht.get_text())
  ax.set_title('')
#htt = ax.text(0.5, 0.98, times[step], ha='center', va='top', transform=plt.gcf().transFigure)
#ax.text(0.02, 0.98, 'ICON-O R2B8', ha='left', va='top', transform=plt.gcf().transFigure, fontsize=14)
ax.text(0.02, 0.01, path_data, ha='left', va='bottom', transform=plt.gcf().transFigure, fontsize=10)
ax.text(0.47, 0.97, 'kinetic energy', ha='center', transform=plt.gcf().transFigure, fontsize=12)
hcb[0].set_ylabel('log$_{10}$ m$^2$/s$^2$', fontsize=8)

def update_fig(step):
  # --- load data
  f = Dataset(flist_ts[step], 'r')
  uo = f.variables['u'][its[step],iz,:]
  vo = f.variables['v'][its[step],iz,:]
  kin = 0.5*(uo**2+vo**2)
  #kin = f.variables['kin'][its[step],iz,:]
  f.close()
  # --- interp to rectgrid 
  kin_rg = pyic.apply_ckdtree(kin, fpath_ckdtree, coordinates='clat clon').reshape(lat_rg.size, lon_rg.size)
  kin_rg[kin_rg==0.] = np.ma.masked
  kin_rg = np.ma.log10(kin_rg)
  # --- update
  for ii in range(len(hca)):
    hm[ii][0].set_array(kin_rg.flatten())
  htt.set_text(times[step])
  return

steps = np.arange(flist_ts.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  ts = pyic.timing(ts, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

ts = pyic.timing(ts, 'all done')

#plt.show()
