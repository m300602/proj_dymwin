import sys
import numpy as np
import matplotlib 
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs
import cartopy
import glob, os
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

run = 'dpp0054'
path_data = f'/work/mh0287/k203123/GIT/icon-aes-dyw3/experiments/{run}/'
timi = pyic.timing([0], 'start')

savefig = True
path_fig = '../movies/%s/' % (__file__.split('/')[-1][:-3])
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass

fpath_tgrid = path_data+'icon_grid_0015_R02B09_G.nc'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9a_res0.10_180W-180E_90S-90N.npz'

# --- load times and flist
#search_str = '2016??/'+run+'_atm_2d_ml_*.nc'
#search_str = run+'_atm_2d_ml_????????.nc'
#search_str = run+'_atm_2d_ml_????????????????.nc'
search_str = run+'_atm_2d_ml_????????????????.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
#flist = flist[41:]
flist = flist[:-1]
times, flist_ts, its = pyic.get_timesteps(flist, time_mode='float2date')
#times, flist_ts, its = pyic.get_timesteps(flist, time_mode='num2date')
#nskip = 4
nskip = 1
times = times[::nskip]
flist_ts = flist_ts[::nskip]
its = its[::nskip]

#times = times[164:]
#flist_ts = flist_ts[164:]
#its = its[164:]

#search_str = run+'

# --- load data
step = 1 # dummy step
timi = pyic.timing(timi, 'load data')
f = Dataset(flist_ts[step], 'r')
hfls = f.variables['hfls'][its[step],:]
hfss = f.variables['hfss'][its[step],:]
ts = f.variables['ts'][its[step],:] - 273.15
f.close()

# --- load ckdtree
timi = pyic.timing(timi, 'apply ckdtree')
ddnpz = np.load(fpath_ckdtree)
lon_rg = ddnpz['lon'] 
lat_rg = ddnpz['lat'] 

# --- first time instance
f = Dataset(flist[0], 'r')
ts0 = f.variables['ts'][:,:].mean(axis=0) - 273.15
f.close()
ts0_rg = pyic.apply_ckdtree(ts0, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
ts0_rg[ts0_rg==0.] = np.ma.masked
ts0_rg = ts0_rg

# --- interpolate to rectgrid
hfls_rg = pyic.apply_ckdtree(hfls, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
hfls_rg[hfls_rg==0.] = np.ma.masked
hfss_rg = pyic.apply_ckdtree(hfss, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
hfss_rg[hfss_rg==0.] = np.ma.masked
ts_rg = pyic.apply_ckdtree(ts, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
ts_rg[ts_rg==0.] = np.ma.masked

tdiff_rg = ts_rg-ts0_rg

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 
plt.close('all')
ccrs_proj = ccrs.PlateCarree()

timi = pyic.timing(timi, 'making axes')
#dpi = 250
hca, hcb = pyic.arrange_axes(2,2, plot_cb=[1,1,1,1], fig_size_fac=2., asp=0.5, projection=ccrs_proj)
ii=-1

timi = pyic.timing(timi, 'reg_1')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm1 = pyic.shade(lon_rg, lat_rg, hfls_rg, ax=ax, cax=cax, clim=500, 
                transform=ccrs_proj, rasterized=False)
ax.set_title('latent heat flux [W/m$^2$]')
bbox=dict(facecolor='w', alpha=1., edgecolor='none')
ht = ax.text(0.02, 0.92, times[step], transform=ax.transAxes, bbox=bbox)

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm2 = pyic.shade(lon_rg, lat_rg, hfss_rg, ax=ax, cax=cax, clim=100, 
                transform=ccrs_proj, rasterized=False)
ax.set_title('sensible heat flux [W/m$^2$]')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm3 = pyic.shade(lon_rg, lat_rg, ts_rg, ax=ax, cax=cax, clim=[-2,30], 
                transform=ccrs_proj, rasterized=False)
ax.set_title('surface temperature [$^o$C]')
ax.text(0.02, 0.01, path_data, ha='left', va='bottom', transform=plt.gcf().transFigure, fontsize=10)

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm4 = pyic.shade(lon_rg, lat_rg, tdiff_rg, ax=ax, cax=cax, clim=5,
                transform=ccrs_proj, rasterized=False)
ax.set_title('surface temperature development [$^o$C]')

# --- make axes nice
#for ax in hca:
#  ax.coastlines()
#  ax.set_xticks(np.arange(-180,180,60), crs=ccrs_proj)
#  ax.set_yticks(np.arange(-90,90,30), crs=ccrs_proj)
#  lon_formatter = LongitudeFormatter()
#  lat_formatter = LatitudeFormatter()
#  ax.xaxis.set_major_formatter(lon_formatter)
#  ax.yaxis.set_major_formatter(lat_formatter)
#  ax.xaxis.set_ticks_position('both')
#  ax.yaxis.set_ticks_position('both')
#hca[0].set_xticklabels([])
#hca[1].set_xticklabels([])
#hca[1].set_yticklabels([])
#hca[3].set_yticklabels([])
for ax in hca:
  pyic.plot_settings(ax, template='global')

def update_fig(step):
  # --- load data
  f = Dataset(flist_ts[step], 'r')
  hfls = f.variables['hfls'][its[step],:]
  hfss = f.variables['hfss'][its[step],:]
  ts = f.variables['ts'][its[step],:] - 273.15
  f.close()
  # --- interp to rectgrid 
  hfls_rg = pyic.apply_ckdtree(hfls, fpath_ckdtree, coordinates='clat clon', 
    radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  hfls_rg[hfls_rg==0.] = np.ma.masked
  hfss_rg = pyic.apply_ckdtree(hfss, fpath_ckdtree, coordinates='clat clon', 
    radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  hfss_rg[hfss_rg==0.] = np.ma.masked
  ts_rg = pyic.apply_ckdtree(ts, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_rg.size, lon_rg.size)
  ts_rg[ts_rg==0.] = np.ma.masked
  tdiff_rg = ts_rg-ts0_rg

  # --- update
  hm1[0].set_array(hfls_rg.flatten())
  hm2[0].set_array(hfss_rg.flatten())
  hm3[0].set_array(ts_rg.flatten())
  #hm4[0].set_array(ts_diff_rg.flatten())
  hm4[0].set_array(tdiff_rg.flatten())
  ht.set_text(times[step])
  return

steps = np.arange(flist_ts.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  timi = pyic.timing(timi, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

timi = pyic.timing(timi, 'all done')

#plt.show()
