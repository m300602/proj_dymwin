import sys
import numpy as np
import matplotlib 
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs
import cartopy
import glob, os
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

run = 'dpp0053'
path_data = f'/work/mh0287/k203123/GIT/icon-aes-dyw3/experiments/{run}/'
timi = pyic.timing([0], 'start')

savefig = True
path_fig = '../movies/%s/' % (__file__.split('/')[-1][:-3])
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass

#fpath_tgrid = path_data+'icon_grid_0015_R02B09_G.nc'
#fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/r2b9a_res0.10_180W-180E_90S-90N.npz'
gname = 'r2b9_oce_r0004'
path_grid = f'/home/mpim/m300602/work/icon/grids/{gname}/'
fpath_tgrid = path_grid+'r2b9_oce_r0004_tgrid.nc'
fpath_ckdtree = path_grid+f'ckdtree/rectgrids/{gname}_res0.10_180W-180E_90S-90N.npz'

# --- load times and flist
search_str = run+'_oce_3d_P1D_????????????????.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
#flist = flist[41:]
flist = flist[:-1]
#times, flist_ts, its = pyic.get_timesteps(flist, time_mode='float2date')
times, flist_ts, its = pyic.get_timesteps(flist, time_mode='num2date')

# --- load data
step = 10 # dummy step
timi = pyic.timing(timi, 'load data')
f = Dataset(flist_ts[step], 'r')
to = f.variables['to'][its[step],0,:]
uo = f.variables['u'][its[step],0,:]
#vo = f.variables['v'][its[step],0,:]
#vort = f.variables['vort'][its[step],0,:]
f.close()

# --- load ckdtree
timi = pyic.timing(timi, 'apply ckdtree')
ddnpz = np.load(fpath_ckdtree)
lon_rg = ddnpz['lon'] 
lat_rg = ddnpz['lat'] 

# --- first time instance
f = Dataset(flist[0], 'r')
sst0 = f.variables['to'][0,0,:]
lon, lat, sst0i = pyic.interp_to_rectgrid(sst0, fpath_ckdtree)
sst0i[sst0i==0.] = np.ma.masked
us0 = f.variables['u'][0,0,:]
lon, lat, us0i = pyic.interp_to_rectgrid(us0, fpath_ckdtree)
us0i[us0i==0.] = np.ma.masked
f.close()

# --- interpolate to rectgrid
lon, lat, toi = pyic.interp_to_rectgrid(to, fpath_ckdtree)
toi[toi==0.] = np.ma.masked
lon, lat, uoi = pyic.interp_to_rectgrid(uo, fpath_ckdtree)
uoi[uoi==0.] = np.ma.masked
#lon, lat, voi = pyic.interp_to_rectgrid(vo, fpath_ckdtree)
#voi[voi==0.] = np.ma.masked
#lon, lat, vorti = pyic.interp_to_rectgrid(vort, fpath_ckdtree, coordinates='vlat vlon')
#vorti[vorti==0.] = np.ma.masked

#kei = 0.5*(uoi**2+voi**2)

tdiff = toi-sst0i
udiff = uoi-us0i

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 
plt.close('all')
ccrs_proj = ccrs.PlateCarree()

timi = pyic.timing(timi, 'making axes')
#dpi = 250
hca, hcb = pyic.arrange_axes(2,2, plot_cb=[1,1,1,1], fig_size_fac=2., asp=0.5, projection=ccrs_proj)
ii=-1

timi = pyic.timing(timi, 'reg_1')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm1 = pyic.shade(lon_rg, lat_rg, toi, ax=ax, cax=cax, clim=[-2,28], 
                transform=ccrs_proj, rasterized=False)
ax.set_title('SST [$^o$C]')
bbox=dict(facecolor='w', alpha=1., edgecolor='none')
ht = ax.text(0.02, 0.92, times[step], transform=ax.transAxes, bbox=bbox)

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm2 = pyic.shade(lon_rg, lat_rg, tdiff, ax=ax, cax=cax, clim=5.0, 
                transform=ccrs_proj, rasterized=False)
ax.set_title('SST development [$^o$C]')

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm3 = pyic.shade(lon_rg, lat_rg, uoi, ax=ax, cax=cax, clim=0.8, 
                transform=ccrs_proj, rasterized=False)
ax.set_title('zonal surface velocity [m/s]')
ax.text(0.02, 0.01, path_data, ha='left', va='bottom', transform=plt.gcf().transFigure, fontsize=10)

# ---
ii+=1; ax=hca[ii]; cax=hcb[ii]
#hm4 = pyic.shade(lon_rg, lat_rg, kei, ax=ax, cax=cax, clim=[0,1], 
#                transform=ccrs_proj, rasterized=False)
#ax.set_title('surface kin. energy [m2/s2]')
hm4 = pyic.shade(lon_rg, lat_rg, udiff, ax=ax, cax=cax, clim=1., 
                transform=ccrs_proj, rasterized=False)
ax.set_title('zonal surface velocity development [m/s]')

# --- make axes nice
for ax in hca:
  pyic.plot_settings(ax, template='global')

def update_fig(step):
  # --- load data
  f = Dataset(flist_ts[step], 'r')
  to = f.variables['to'][its[step],0,:]
  uo = f.variables['u'][its[step],0,:]
  #vo = f.variables['v'][its[step],0,:]
  f.close()
  # --- interp to rectgrid 
  lon, lat, toi = pyic.interp_to_rectgrid(to, fpath_ckdtree)
  toi[toi==0.] = np.ma.masked
  lon, lat, uoi = pyic.interp_to_rectgrid(uo, fpath_ckdtree)
  uoi[uoi==0.] = np.ma.masked
  #lon, lat, voi = pyic.interp_to_rectgrid(vo, fpath_ckdtree)
  #voi[voi==0.] = np.ma.masked

  tdiff = toi-sst0i
  udiff = uoi-us0i

  #kei = 0.5*(uoi**2+voi**2)

  # --- update
  #hm1[0].set_array(toi[1:,1:].flatten())
  #hm2[0].set_array(tdiff[1:,1:].flatten())
  #hm3[0].set_array(uoi[1:,1:].flatten())
  #hm4[0].set_array(udiff[1:,1:].flatten())
  hm1[0].set_array(toi.flatten())
  hm2[0].set_array(tdiff.flatten())
  hm3[0].set_array(uoi.flatten())
  hm4[0].set_array(udiff.flatten())
  ht.set_text(times[step])
  return

steps = np.arange(flist_ts.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  timi = pyic.timing(timi, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

timi = pyic.timing(timi, 'all done')

#plt.show()
