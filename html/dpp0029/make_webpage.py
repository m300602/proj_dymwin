import sys, glob, os
from pyicon.quickplots import QuickPlotWebsite
import shutil
import datetime
import numpy as np
import matplotlib.pyplot as plt

path_base = './'
path_pics = './pics/'

plots = []
plots += ['sec:Surface']
plots += ['dymwin_bias_en4_sst_sss']
plots += ['sec:Interior']

Dplots = dict()
Dplots['sec:Surface'] = ''
Dplots['dymwin_bias_en4_ssh'] = 'SSH'
Dplots['dymwin_bias_en4_sst_sss'] = 'Surface temp and salt'
Dplots['dymwin_bias_en4_sst_bias'] = 'SST bias'
Dplots['dymwin_bias_en4_sss_bias'] = 'SSS bias'
Dplots['icon_r2b9_ke_glob_reg_mov'] = 'Kin. energy snapshot'
Dplots['icon_r2b9_mld_glob_reg_mov'] = 'MLD snapshot'
Dplots['sec:Ice'] = ''
Dplots['dymwin_bias_en4_ice_thickness_nh'] = 'Ice Thickness NH'
Dplots['dymwin_bias_en4_ice_concentration_nh'] = 'Ice Concentration NH'
Dplots['dymwin_bias_en4_ice_thickness_sh'] = 'Ice Thickness SH'
Dplots['dymwin_bias_en4_ice_concentration_sh'] = 'Ice Concentration SH'
Dplots['sec:Interior'] = ''
Dplots['dymwin_bias_en4_tbias_zave'] = 'Temperature bias zon. ave.'
Dplots['dymwin_bias_en4_tbias_azave'] = 'Temperature bias Atl. zon. ave.'
Dplots['dymwin_bias_en4_sbias_zave'] = 'Salinity bias zon. ave.'
Dplots['sec:Equator'] = ''
Dplots['dymwin_bias_en4_tbias_eq_along_sec'] = 'Temp bias along equator'
Dplots['dymwin_bias_en4_uo_eq_across'] = 'Zon. velocity and temp'
Dplots['dymwin_bias_en4_tbias_eq_across'] = 'Temp. bias across equator and zon. velocity'

name = 'dpp0029'


# --- make web page
qp = QuickPlotWebsite(
  title=f'{name}', 
  author=os.environ.get('USER'),
  date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
  path_data='some info possible',
  #info=f'time average from {t1} to {t2}',
  fpath_css='./qp_css.css',
  fpath_html=path_base+'index.html',
  links='&emsp; <a href="../index.html">list simulations</a>',
  )

for fig_name in Dplots.keys():
  print(f'Linking {fig_name}')
  if fig_name.startswith('sec'):
    qp.add_section(fig_name.split(':')[1])
  elif os.path.exists(path_pics+fig_name+'.html'): 
     qp.add_subsection(Dplots[fig_name])
     qp.add_html(f'{path_base}pics/{fig_name}.html')
  elif os.path.exists(path_pics+fig_name+'.png'): 
     qp.add_subsection(Dplots[fig_name])
     qp.add_fig(f'pics/{fig_name}.png')
  else:
    print(f'::: Warning: {fig_name} could not be found.:::')
qp.write_to_file()
