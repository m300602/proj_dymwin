import sys, glob, os
from pyicon.quickplots import QuickPlotWebsite
import shutil
import datetime
import numpy as np
import matplotlib.pyplot as plt

path_base = './'
path_pics = './pics/'

plots = []
plots += ['sec:Surface']
plots += ['r2b9_dymwin_spinup_sst_sss']
plots += ['sec:Interior']

Dplots = dict()
Dplots['sec:Surface'] = ''
Dplots['r2b9_dymwin_spinup_ssh'] = 'SSH'
Dplots['r2b9_dymwin_spinup_sst_sss'] = 'Surface temp and salt'
Dplots['r2b9_dymwin_spinup_sst_bias'] = 'SST bias'
Dplots['r2b9_dymwin_spinup_sss_bias'] = 'SSS bias'
Dplots['icon_r2b9_ke_glob_reg_mov'] = 'Kin. energy snapshot'
Dplots['icon_r2b9_mld_glob_reg_mov'] = 'MLD snapshot'
Dplots['sec:Ice'] = ''
Dplots['r2b9_dymwin_spinup_ice_thickness_nh'] = 'Ice Thickness NH'
Dplots['r2b9_dymwin_spinup_ice_concentration_nh'] = 'Ice Concentration NH'
Dplots['r2b9_dymwin_spinup_ice_thickness_sh'] = 'Ice Thickness SH'
Dplots['r2b9_dymwin_spinup_ice_concentration_sh'] = 'Ice Concentration SH'
Dplots['sec:Interior'] = ''
Dplots['r2b9_dymwin_spinup_tbias_zave'] = 'Temperature bias zon. ave.'
Dplots['r2b9_dymwin_spinup_tbias_azave'] = 'Temperature bias Atl. zon. ave.'
Dplots['r2b9_dymwin_spinup_sbias_zave'] = 'Salinity bias zon. ave.'
Dplots['sec:Equator'] = ''
Dplots['r2b9_dymwin_spinup_tbias_eq_along_sec'] = 'Temp bias along equator'
Dplots['r2b9_dymwin_spinup_uo_eq_across'] = 'Zon. velocity and temp'
Dplots['r2b9_dymwin_spinup_tbias_eq_across'] = 'Temp. bias across equator and zon. velocity'

name = 'exp.ocean_era51h_zstar_r2b9_21182-DYW'


# --- make web page
qp = QuickPlotWebsite(
  title=f'{name}', 
  author=os.environ.get('USER'),
  date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
  path_data='some info possible',
  #info=f'time average from {t1} to {t2}',
  fpath_css='./qp_css.css',
  fpath_html=path_base+'index.html',
  links='&emsp; <a href="../index.html">list simulations</a>',
  )

for fig_name in Dplots.keys():
  print(f'Linking {fig_name}')
  if fig_name.startswith('sec'):
    qp.add_section(fig_name.split(':')[1])
  elif os.path.exists(path_pics+fig_name+'.html'): 
     qp.add_subsection(Dplots[fig_name])
     qp.add_html(f'{path_base}pics/{fig_name}.html')
  elif os.path.exists(path_pics+fig_name+'.png'): 
     qp.add_subsection(Dplots[fig_name])
     qp.add_fig(f'pics/{fig_name}.png')
  else:
    print(f'::: Warning: {fig_name} could not be found.:::')
qp.write_to_file()
